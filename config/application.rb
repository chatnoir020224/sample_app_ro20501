require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module SampleApp
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    # レスポンスのテストはRequest Specが推奨されている
    # https://morizyun.github.io/ruby/rspec-rails-request.html
    config.generators do |g|
      g.test_framework :rspec,
                        fixtures: true,
                        controller_specs: true,
                        helper_specs: false,
                        routing_specs: false                        
    end
  end
end
