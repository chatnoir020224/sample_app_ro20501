require 'rails_helper'

RSpec.describe "AccessToStaticPages", type: :request do
  # 参考URL
  # https://qiita.com/sakakinn/items/9c4d6e278cd19867f80f
  # http://www.code-magagine.com/?p=7773
  let(:page_name) {""}
  let(:base_title) {"#{page_name}Ruby on Rails Tutorial Sample App"}

  describe "GET /access_to_static_pages" do
    context "Get root" do
      let(:page_name) {"Home"}

      before do
        get root_url
      end

      it "ステータス:200(成功)を返すこと" do
        expect(response).to have_http_status(200)
      end
      
    end
    context "Get #home" do

      before do
        get static_pages_home_url
      end

      it "ステータス:200(成功)を返すこと" do
        expect(response).to have_http_status(200)
      end

      it "Headerタイトルが'Home | Ruby on Rails Tutorial Sample App'であること" do
        expect(response.body).to include base_title
      end
    end

    context "Get #help" do
      let(:page_name) {"Help | "}
      before do
        get static_pages_help_url
      end

      it "ステータス:200(成功)を返すこと" do
        expect(response).to have_http_status(200)
      end

      it "Headerタイトルが'Help | Ruby on Rails Tutorial Sample App'であること" do
        expect(response.body).to include base_title
      end
    end

    context "Get #about" do
      let(:page_name) {"About | "}
      before do
        get static_pages_about_url
      end

      it "ステータス:200(成功)を返すこと" do
        expect(response).to have_http_status(200)
      end

      it "Headerタイトルが'About | Ruby on Rails Tutorial Sample App'であること" do
        expect(response.body).to include base_title
      end
    end

    context "Get #contact" do
      let(:page_name) {"Contact | "}
      before do
        get static_pages_contact_url
      end
      it "ステータス:200(成功)を返すこと" do
        expect(response).to have_http_status(200)
      end

      it "Headerタイトルが'Contact | Ruby on Rails Tutorial Sample App'であること" do
        expect(response.body).to include base_title
      end
    end
  end
end
